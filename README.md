# Proyecto Aeromexico

Este proyecto se inició con un *template* que se construyó de forma independiente al subproyecto frontend, permitiendonos avanzar por separado con la forma habitual de trabajo de cada desarrollador.

Se integró después en dicho subproyecto desarrollado en angularJS dentro de la carpeta frontend. En el momento en que el markup general y las hojas de estilos estuvieron bastante avanzadas y se empezó el periodo de los ajustes, en ese momento fue que se dio por cerrado el template.

En la carpeta backend se tiene el desarrollo del subproyecto con el mismo nombre donde se utilizó PHP y composer para el manejo de librerías y proyectos públicos necesarios para su funcionamiento. Al final el backend únicamente almacena el registro de usuarios participantes a los cuales se les envió correo confirmando la fecha en que podían asistir. Sin embargo tiene más archivos que con más tiempo para su desarrollo podrían funcionar para multiples proyectos.

Se implemento una instalación de WP para el almacenamiento de forma que rápidamente pudimos tener un sitema auto administrable, se ajustaron algunos archivos para cargar todo atravez de servicios rest y se hizo un lincado al WP desde el proyecto frontend para cargar los metadatos necesarios para Facebook openGraph.

Frontend está construido con el generador generator-nodes que facilitó crear la estructura, utiliza bower para cargar las librerías/bibliotecas/apis (como se quieran llamar), reduciendo la carga de información que se guarda en el repositorio y permitiendo clonar de forma rápida y sencilla este proyecto.

backend utiliza composer, auque algunas librerías no se encuentran registradas en este XXXXXX composer permite hacer un *registro* local indicando la url del repositorio y los archivos a cargar.
Utiliza phpmailer, php_crud_api, etc.

## Instrucciones para el uso de frontend

Para poder trabajar con el proyecto de frontend es necesario tener instalado nodeJS y en un terminal dentro de la carpeta frontend se ejecutan las siguientes instrucciones:

*  Dependencias
    ```
    npm install -g yo generator-nodes bower grunt-cli
    ```
*  Traer las librerías necesarias
    ```
    npm install
    bower install
    ```
*  Para probar el desarrollo de forma local
    ```
    grut serve
    ```

Para mayor referencia sobre el generator utilizado visitar [generator-nodes](https://github.com/nodes-frontend/generator-nodes)

Para mandar a producción, liberar, subir por ftp a servidor, como se quiera decir...
```
grunt build-production
```
Copiar el contenido de la carpeta **dist** al servidor. Las imagenes en la carpeta *assets/images* solo se permite un nivel de subcarpetas, por ejemplo *images/home/xxxxx.jpg*, si se tienen más niveles, digamos *images/home/**products**/xxxxx.jpg*, esta carpeta no se copia a *dist* y hay que llevarla desde *app/assets/images/*.

## Backend
La carpeta backend requiere de la instalación de composer para el registro de personas que asistirán al evento y algunas herramientas para los productos en el home.
```
composer install
```
traerá las dependencias necesarias

EL archivo index.php es crucial para la carga de metadatos.
*El archivo .htaccess de la carpeta raiz del front debe apuntar todo el trafico hacia este archivo index.php*
